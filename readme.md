## Zadanie 1
Napisz w języku PHP (7.*) klasę, które będzie przyjmowała podawane kolejno liczby naturalne, a na żądanie użytkownika będzie pozwalała na uzyskanie informacji, czy większa była trzykrotność liczb parzystych, czy dwukrotność nieparzystych.

## Zadanie 2
Utwórz poprawny szkielet strony internetowej w oparciu o standard HTML5, zawierający następujące
elementy:

1. Element DIV o identyfikatorze “menu
2. Twoje CV - umieść jego zawartość wewnątrz odpowiednich tagów
3. Formularz zawierający następujące pola:

        * imię
        * nazwisko
        * wiek
        * płeć
        * wyrażenie zgody na przetwarzanie danych osobowych

## Zadanie 3
Korzystając z biblioteki jQuery (bez wykorzystania dodatkowych skryptów/bibliotek) napisz walidację do
formularza z zadania numer 2.

Walidacja powinna obejmować:
* wymagane pola - wszystkie
* wiek - od 18 do 99 lat

## Zadanie 4
Napisz kod SQL, który utworzy tabelę o nazwie“menu”, w której możliwe będzie przechowanie menu o
następującej strukturze:

    Kategoria główna
        Podkategoria 1
            Produkt a
            Produkt B
        Podkategoria 2
            Produkt C
            Produkt D

Utwórz zapytania, które dodadzą w/w wartości do tabeli.

## Zadanie 5
Napisz skrypt PHP który pobierze rekurencyjnie dane z tabeli *"menu"* i wyświetli na stworzonej stronie
wewnątrz elementu o id="menu" (zachowując hierarchię). Sposób prezentacji poziomu zagłębienia dowolny.

## Zadanie 6
Korzystając z Doctrine utwórz model opisujący produkt w sklepie internetowym. Produkt powinien być
opisany przez następujące pola:

    nazwa - wymagana
    opis
    cena - wymagana
    dostępność - bool

Utwórz klasę repozytorium dla tego modelu, która będzie posiadała następujące metody:

    zwracającą liczbę dostępnych produktów
    zwracającą kolekcję niedostępnych produktów
    zwracającą kolekcję produktów, które w nazwie posiadają zadaną frazę

## Wysłanie rozwiązania

*   Stwórz prywatne repozytorium na [bitbucket.org](https://bitbucket.org)
*   W branchu _master_ umieść niezmienioną zawartość tego zadania, a swoją pracę
    w dowolnym innym branchu.
*   Otwórz pull-request z brancha z rozwiązaniem do brancha master. Będziemy go 
    oglądać i komentować.
*   Dodaj prawo odczytu do swojego repozytorium użytkownikowi `goldensubmarine` i dodaj
    go jako review'era pull-request'a.
    

Wykonaj zadanie najlepiej jak potrafisz. Ważniejsza jest dla nas jakość kodu niż szybkie odesłanie rozwiązania.
